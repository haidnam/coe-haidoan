﻿using OOPDemo.Impl;
using OOPDemo.Model;
using System;

namespace OOPDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Race race = new Race();
            race.Begin();

            Shop shop = new Shop();
            shop.CompleteOrder();
            shop.CompletFakeOrder();
        }
    }
}
