﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPDemo.Interfaces
{
    public interface IEmailSender
    {
        void SendMessage(string receiver, string title, string message);
    }
}
