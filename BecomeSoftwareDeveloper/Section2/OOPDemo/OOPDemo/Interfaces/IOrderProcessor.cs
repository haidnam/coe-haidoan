﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPDemo.Interfaces
{
    public interface IOrderProcessor
    {
        void ProcessOrder(string email, int orderId);
    }
}
