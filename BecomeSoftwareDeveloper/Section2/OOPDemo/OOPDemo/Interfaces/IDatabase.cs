﻿using OOPDemo.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOPDemo.Interfaces
{
    public interface IDatabase
    {
        bool IsConnected { get; }
        void Connect();
        User GetUser(string email);
        Order GetOrder(int id);
        void SaveChanges();
    }
}
