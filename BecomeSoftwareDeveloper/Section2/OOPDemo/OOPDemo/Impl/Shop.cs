﻿using OOPDemo.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOPDemo.Impl
{
    public class Shop
    {
        public void CompleteOrder()
        {
            IDatabase database = new Database();
            IEmailSender emailSender = new EmailSender();
            IOrderProcessor orderProcessor = new OrderProcessor(database, emailSender);
            orderProcessor.ProcessOrder("user1@email.com", 1);
        }

        public void CompletFakeOrder()
        {
            IDatabase database = new FakeDatabase();
            IEmailSender emailSender = new FakeEmailSender();
            IOrderProcessor orderProcessor = new OrderProcessor(database, emailSender);
            orderProcessor.ProcessOrder("user1@email.com", 1);
        }
    }
}
