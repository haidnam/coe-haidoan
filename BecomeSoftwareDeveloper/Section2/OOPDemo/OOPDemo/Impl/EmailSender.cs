﻿using OOPDemo.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOPDemo.Impl
{
    public class EmailSender : IEmailSender
    {
        public void SendMessage(string receiver, string title, string message)
        {
            Console.WriteLine("Sending a real email message...");
        }
    }
}
