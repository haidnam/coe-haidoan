﻿using OOPDemo.Interfaces;
using OOPDemo.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOPDemo.Impl
{
    public class OrderProcessor : IOrderProcessor
    {
        private readonly IDatabase _database;
        private readonly IEmailSender _emailSender;

        public OrderProcessor(IDatabase database, IEmailSender emailSender)
        {
            _database = database;
            _emailSender = emailSender;
        }

        public void ProcessOrder(string email, int orderId)
        {
            User user = _database.GetUser(email); //Fetch from db
            Order order = _database.GetOrder(orderId); //Fetch from db
            Console.WriteLine($"Processing order with id: {orderId} for the user: '{user.Email}'");
            user.PurchaseOrder(order);
            _database.SaveChanges();
            _emailSender.SendMessage(email, "Order purchased", "You've purchased an order.");
        }
    }
}
