﻿using OOPDemo.Interfaces;
using OOPDemo.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOPDemo.Impl
{
    public class Database : IDatabase
    {
        public bool IsConnected { get; private set; }

        public void Connect()
        {
            Console.WriteLine("Connecting to the real database...");
            IsConnected = true;
        }

        public Order GetOrder(int id)
        {
            return new Order(1, 100);
        }

        public User GetUser(string email)
        {
            return new User(email, "secret");
        }

        public void SaveChanges()
        {
            Console.WriteLine("Saving changes in the real database...");
        }
    }
}
