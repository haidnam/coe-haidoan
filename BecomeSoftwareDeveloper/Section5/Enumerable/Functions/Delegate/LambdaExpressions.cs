﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Delegate.Functions
{
    public class LambdaExpressions
    {
        public delegate void Writer();

        public void Test()
        {
            // Writer writer = Write;
            Action writer = () => Console.WriteLine("Writing...");
            Action<string, int> advancedWriter = (str, num) => Console.WriteLine($"{str}, {num}");
            writer();
            advancedWriter("Hello!", 1);

            Func<int, int, int> adder = (x, y) => x + y;
            var sum = adder(1, 2);
            Console.WriteLine($"Add lambda expression has returned the sum of the two numbers: {sum}.");

            Action<int, string> logger = (t, m) =>
            {
                Console.WriteLine($"Temperature is at: {t}. {m}");
            };

            CheckTemperature(t => logger(t, "Too low!"), t => logger(t, "Optimal."), t => logger(t, "Too high!"));
        }

        public static void Write()
        {
            Console.WriteLine("Writing...");
        }

        public static void CheckTemperature(Action<int> tooLow, Action<int> optimal, Action<int> tooHigh)
        {
            var temperature = 10;
            var random = new Random();

            while (true)
            {
                var change = random.Next(-5, 5);
                temperature += change;
                if (temperature <= 0)
                {
                    tooLow(temperature);
                }
                else if (temperature > 0 && temperature <= 10)
                {
                    optimal(temperature);
                }
                else
                {
                    tooHigh(temperature);
                }

                Thread.Sleep(500);
            }
        }
    }
}
