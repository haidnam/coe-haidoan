﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EnumerableExample.Functions.Extensions
{
    public static class Extensions
    {
        public static bool Empty(this string value)
        {
            return string.IsNullOrWhiteSpace(value);
        }

        public static bool NotEmpty(this string value)
        {
            return !value.Empty();
        }
    }

    public class ExtensionTest
    {
        public void Test()
        {
            var text = "hello";
            if (text.Empty())
            {
                // Do something useful here
            }
        }
    }
}
