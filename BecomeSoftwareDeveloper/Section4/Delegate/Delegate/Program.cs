﻿using Delegate.Functions;
using System;

namespace Delegate
{
    class Program
    {
        static void Main(string[] args)
        {
            var functionalSandbox = new FunctionalSandbox();
            functionalSandbox.Test();
        }
    }
}
