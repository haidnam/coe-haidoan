﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Delegate.Models
{
    public class Race
    {
        public void Begin()
        {
            Car sportCar = new SportCar();
            Car truck = new Truck();

            List<Car> cars = new List<Car>
            {
                sportCar, truck
            };

            foreach (Car car in cars)
            {
                car.Start();
                car.Accelerate();
                car.Boost();
            }
        }

        public void Casting()
        {
            Car sportCar = new SportCar();
            Car truck = new Truck();

            SportCar castedSportCar = sportCar as SportCar;
            if (castedSportCar != null)
            {
                castedSportCar.DisplayInfo();
            }
        }
    }
}
