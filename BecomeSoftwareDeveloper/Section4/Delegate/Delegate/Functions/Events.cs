﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Delegate.Functions
{
    public class Events
    {
        public delegate void UpdateStatus(string status);

        public event UpdateStatus StatusUpdated;
        public EventHandler<StatusEventArgs> StatusUpdatedAgain;

        public void Test()
        {

        }

        public void StartUpdatingStatus()
        {
            while (true)
            {
                if (StatusUpdated != null)
                {
                    StatusUpdated($"status with ticks {DateTime.UtcNow.Ticks}");
                }
                StatusUpdated?.Invoke($"status with ticks {DateTime.UtcNow.Ticks}");
                StatusUpdatedAgain?.Invoke(this, new StatusEventArgs("Status event args"));
                Thread.Sleep(500);
            }
        }
    }
}
