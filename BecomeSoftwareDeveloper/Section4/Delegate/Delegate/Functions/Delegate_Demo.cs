﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Delegate
{
    public class Delegate_Demo
    {
        public delegate int Add(int x, int y);

        public static int AddTwoNumbers(int a, int b)
        {
            return a + b;
        }
        public void Test()
        {
            Add adder = AddTwoNumbers;
            var result = adder(1, 2);
        }
    }
}
