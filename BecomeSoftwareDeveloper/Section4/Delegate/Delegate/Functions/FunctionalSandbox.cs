﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Delegate.Functions
{
    public class FunctionalSandbox
    {
        public void Test()
        {
            var delegates = new Delegates();
            delegates.Test();

            var lambdaExpressions = new LambdaExpressions();
            lambdaExpressions.Test();

            var events = new Events();
            events.StatusUpdated += s =>
            {
                Console.WriteLine($"Received: {s}");
            };
            events.StatusUpdated += DisplayStatus;
            events.StatusUpdated += DisplayStatus2;
            events.StatusUpdatedAgain += (sender, eventArgs) =>
            {
                Console.WriteLine($"Event handler callback: {eventArgs.Status}");
            };
            events.StartUpdatingStatus();
        }

        public void DisplayStatus(string status)
        {
            Console.WriteLine($"Received: {status}");
        }

        public void DisplayStatus2(string status)
        {
            Console.WriteLine($"Received again: {status}");
        }
    }
}
