﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Delegate.Functions
{
    public class StatusEventArgs : EventArgs
    {
        public string Status { get; }
        public StatusEventArgs(string status)
        {
            Status = status;
        }
    }
}
