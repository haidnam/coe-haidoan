﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Section3.Model
{
    public class Truck : Car
    {
        public override void Accelerate()
        {
            Console.WriteLine("Accelerating a truck...");
            base.Accelerate();
        }

        public override void Boost()
        {
            Console.WriteLine("Boosting a truck...");
            Speed += 50;
            Console.WriteLine($"Running at: {Speed} km/h.");
        }
    }
}
