﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Section3.Model
{
    public abstract class Car
    {
        public double Aceeleration { get; protected set; } = 10;
        public double Speed { get; protected set; } = 100;

        public void Start()
        {
            Console.WriteLine("Turning on the engine...");
            Console.WriteLine($"Running at: {Speed} km/h.");
        }


        public void Stop()
        {
            Console.WriteLine("Stopping the car...");
        }

        public virtual void Accelerate()
        {
            Console.WriteLine("Accelerating...");
            Speed += Aceeleration;
            Console.WriteLine($"Running at: {Speed} km/h.");
        }

        public abstract void Boost();
    }





}
