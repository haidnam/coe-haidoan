﻿using Section3.Model;
using System;

namespace Section3
{
    class Program
    {
        static void Main(string[] args)
        {
            var user = new User("user@email.com", "secret");

            var anotherUser = new
            {
                Id = 1,
                Name = "user",
                Address = new
                {
                    Street = "Krakowska 1",
                    City = "Kraków"
                }
            };
        }
    }
}
