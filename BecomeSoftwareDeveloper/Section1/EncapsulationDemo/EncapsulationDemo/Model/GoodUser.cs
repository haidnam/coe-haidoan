﻿using System;

namespace EncapsulationDemo.Model
{
    public class GoodUser
    {
        public string Email { get; private set; }
        public string Password { get; private set; }

        //Let's assume that we don't care about the value here.
        public string FirstName { get; set; }

        //And the same way of thinking does apply here.
        public string LastName { get; set; }

        public int Age { get; private set; }
        public bool IsActive { get; private set; }
        public DateTime UpdatedAt { get; private set; }
        public decimal Funds { get; private set; }

        public GoodUser(string email, string password)
        {
            SetEmail(email);
            SetPassword(password);
            Activate();
        }

        private void SetEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new Exception("Email can not be empty.");
            }
            if (Email == email)
            {
                return;
            }
            Email = email;
            Update();
        }

        public void AddFunds(decimal funds)
        {
            Funds += funds;
        }

        public void SetPassword(string password)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new Exception("Password can not be empty.");
            }
            if (Password == password)
            {
                return;
            }
            Password = password;
            Update();
        }

        public void SetFirstName(string firstName)
        {
            if (FirstName == firstName)
            {
                return;
            }
            FirstName = firstName;
            Update();
        }

        public void SetLastName(string lastName)
        {
            if (LastName == lastName)
            {
                return;
            }
            LastName = lastName;
            Update();
        }

        public void SetAge(int age)
        {
            if (age < 13)
            {
                throw new Exception("Age can not be lesser than 13 years.");
            }
            if (Age == age)
            {
                return;
            }
            Age = age;
            Update();
        }

        public void Activate()
        {
            if (IsActive)
            {
                return;
            }
            IsActive = true;
            Update();
        }

        public void Deactivate()
        {
            if (!IsActive)
            {
                return;
            }
            IsActive = false;
            Update();
        }

        public void PurchaseOrder(Order order)
        {
            if (!IsActive)
            {
                throw new Exception("Only active user can purchase an order.");
            }

            decimal orderPrice = order.TotalPrice;
            if (Funds - orderPrice < 0)
            {
                throw new Exception("You don't have enough money to purchase an order.");
            }
            order.Purchase();
            Funds -= orderPrice;
        }

        public bool ValidatePassword(string password)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new Exception("Password to be validated can not be empty.");
            }

            return Password == password;
        }

        private void Update()
        {
            UpdatedAt = DateTime.UtcNow;
        }
    }
}