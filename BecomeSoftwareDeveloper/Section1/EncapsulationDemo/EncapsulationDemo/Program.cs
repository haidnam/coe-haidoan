﻿using EncapsulationDemo.Model;
using System;
using System.Collections.Generic;

namespace EncapsulationDemo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //This BadUser class is so wrong on so many levels...
            //However such classes so-called POCO (Plain Old CLR Object)...
            //Have their use e.g. as DTO (Data Transfer Object), ViewModel, Database table mapping etc.
            User badUser = new User();
            badUser.Email = "a";

            //The GoodUser represents a much better "domain" model.
            GoodUser goodUser = new GoodUser("good@user.com", "secret");

            //Let's do some simple logic here and purchase a few orders.
            Login(goodUser, "secret");
            List<Order> orders = GetOrders();
            goodUser.AddFunds(40);

            foreach (Order order in orders)
            {
                goodUser.PurchaseOrder(order);
                DisplayFunds(goodUser, order);
            }
        }

        static void Login(GoodUser user, string password)
        {
            if (!user.ValidatePassword(password))
            {
                throw new Exception("Invalid credentials.");
            }
            Console.WriteLine($"User: {user.Email} was authenticated...");
        }

        static List<Order> GetOrders()
        {
            return new List<Order>
            {
                new Order(1, 10),
                new Order(2, 20)
            };
        }

        static void DisplayFunds(GoodUser user, Order order)
        {
            Console.WriteLine($"Funds after purchasing order {order.Id}: {user.Funds}.");
        }
    }
}
